# SPDX-FileCopyrightText: 2023 Coop IT Easy SC <https://coopiteasy.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Exceptions definition"""


class ConfigError(Exception):
    """Exception when using config file"""
