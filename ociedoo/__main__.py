# SPDX-FileCopyrightText: 2018 Coop IT Easy SC <https://coopiteasy.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Executed when module is run as a script."""

from ociedoo import cli

cli.main()  # pylint: disable=no-value-for-parameter
