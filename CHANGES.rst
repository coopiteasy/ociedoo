0.7.0 (2022-02-15)
==================

Features
--------

- The connection to the databases are now closed before trying to rename
  the database. When dropping a database, to close connection, the --force
  option should be given. (free-busy-db)
- Remove mimetype dependency. Backup mimetype are no longer checked before
  trying to restore. (mimetype)
- Configuration file is now in toml language and it's structure has
  changed.
  Now configuration item are separated in several profile. The first
  profile is the default one. Other profile can be selected using option
  in command line. (new-config)
- update-module command has been changed. Now, it perform multiple updates
  in different process. Also you can chose if restart must occur before
  or during the update process. Also you can chose to not restart odoo at
  all. Finally, databases to update are inferred from the list of modules
  that need to be updated. (update-module)


0.6.0 (2020-04-03)
==================

* New command set-password to change the password of a user of an odoo
  database. By default this user is the user with login 'admin'.
* `set-password` can be used also with the `restore-db` command.
* `restore-db` command now ask for a new password for 'admin' user by
  default. `--no-set-password` can be given to not change password.
* New command `list-module` that list installed module on a database.
* Databases listed by `list-db` can now by filtered by installed
  modules.
* New command `copy-db` to easy copy an existing database with
  completion.
* New command `drop-db` to safely delete a database with completion.


0.5.0 (2019-10-21)
==================

* Add a config command that let you inspect your configuration files.
* Add commands to manage Odoo daemon.
* Now autocompletion is available also for regular files, and there is
  some fix about file completion in the same directory.
* Now license and description is correctly shown when doing a `pip show
  ociedoo`.
* Now, the posthook are correctly activated and the `--disable-posthook`
  works as expected.
* Improve README and add recommended installation process with pipx.
* Improvement done in the way external command are run and the way
  errors are managed.
* Improve help.
* `run` command now correctly check about existing profile.


0.4.4 (2019-06-12)
==================

* First attempt to fix `stop_daemon` function by letting some time to
  the daemon to stop.


0.4.3 (2019-05-01)
==================

* In `restore-db` fix the procedure to change the admin password of a
  DB.


0.4.2 (2019-04-27)
==================

* In `update-module`, a fix that let you use the `--pager` option.
* In `update-module`, fix message when failing to stop daemon.
* In `update-module`, a fix that let you correctly interrupt ociedoo via
  CTRL-C.
* In `run`, a fix that let you correctly interrupt ociedoo via CTRL-C.


0.4.1 (2019-04-27)
==================

* Add installation procedure for dependencies in README.
* Turning odoo on and off does not required to type empty password any
  more. Empty password is assumed.
* Fix for `update-module` where extra empty line was added in the odoo
  output. Now output looks like the original one.
* Fix for `update-module` better catching errors.
* Fix for `update-module` where the daemon configured in the
  configuration file was not stopped.
* Fix package dependencies for pip installation
* Move project to a new dedicated repository


0.4.0 (2019-04-24)
==================

* Add to `restore-db` a option to set admin password for the restored
  database.
* Add to `restore-db` the possibility to execute SQL after the restore
  of a database. This allows you to specify an SQL file (or a gziped
  version of it) that will be applied after a database restore. This is
  called posthook. A posthook can be set in the configuration file (in
  the section "restore") or given in the command line as a positional
  argument. Posthooks can be disabled via the `--disable-posthook` flag.
* Autosave flag for `restore-db` can now be specified in the
  configuration file in the "restore" section.
* Fix issue in filetype check for sql file.
* Add pylint check for a better code quality.
* Improve help messages.
* Add a command named `run`. It allows you run odoo with special default
  options. Theses options are put in a 'profile'. Multiple profile can
  be created. For more info, see: `ociedoo run --help`.
* Now the configuration file is also searched in the current execution
  directory. This allow you to put a config file ('ociedoorc') in the
  execution directory. Really useful to have a config file for a project
  that extend a user config file, without specifying each time in the
  command line.


0.3.0 (2019-04-18)
==================

* Fix file hierarchy to be compliant with pip.
* New command 'rename-db' that let you safely rename dbs.
* New command 'restore-db' that let you easily and safely restore a
  db.
* Refactor of the old 'update-module' command. It can now turn on
  and off the odoo daemon, prevent creating an empty db and let use
  a pager.
* Autocompletion for databases and modules.
* Better help.


0.2.0 (2019-04-10)
==================

* Refactor for using click python library.
* Add subcommand `list-db`.
* Add default value for configuration file.


0.1.0 (2019-04-07)
==================

* Add configuration file management.
