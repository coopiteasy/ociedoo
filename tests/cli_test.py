# SPDX-FileCopyrightText: 2018 Coop IT Easy SC <https://coopiteasy.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test for cli.py file"""

import collections
import os
import tempfile
from pathlib import Path

from ociedoo import complete, config, lib


def test_get_addons_path():
    """
    Test for :
        - lib.get_addons_path
        - complete.modules_complete
    """
    manifest = "__manifest__.py"
    openerp = "__openerp__.py"
    with tempfile.TemporaryDirectory("-ociedoo-test") as tmpdir:
        # Create directory referenced in the config file
        odoo_addons_dir = Path(tmpdir) / Path("odoo") / Path("addons")
        odoo_addons_dir.mkdir(parents=True, exist_ok=True)
        oca_server_dir = Path(tmpdir) / Path("oca") / Path("server")
        oca_server_dir.mkdir(parents=True, exist_ok=True)
        oca_test_dir = Path(tmpdir) / Path("oca") / Path("test")
        oca_test_dir.mkdir(parents=True, exist_ok=True)

        odoo_addons_mod = []
        for i in range(3):
            odoo_addons_mod.append(odoo_addons_dir / Path("mod%d" % i))
            odoo_addons_mod[i].mkdir()
            (odoo_addons_mod[i] / Path(manifest)).touch()
        odoo_addons_mod.append(odoo_addons_dir / Path("nonmod"))
        odoo_addons_mod[-1].mkdir()

        oca_server_mod = []
        for i in range(4):
            oca_server_mod.append(oca_server_dir / Path("ocamod%d" % i))
            oca_server_mod[i].mkdir()
            (oca_server_mod[i] / Path(openerp)).touch()
        oca_server_mod.append(oca_server_dir / Path("nonmod"))
        oca_server_mod[-1].mkdir()

        oca_test_mod = []
        for i in range(5):
            oca_test_mod.append(oca_test_dir / Path("nonmod%d" % i))
            oca_test_mod[i].mkdir()

        # Create default config file
        odoo_conf = Path(tmpdir) / Path("odoo_conf")
        odoo_conf.write_text(
            """[options]
addons_path =
    odoo/addons,
    oca/server,
    %s,
    ~
            """
            % oca_test_dir
        )
        addons_path = lib.get_addons_path(str(odoo_conf), str(tmpdir))
        assert addons_path == [
            str(odoo_addons_dir),
            str(oca_server_dir),
            str(oca_test_dir),
            str(Path().home()),
        ]

        # ociedoo config file for testing
        Context = collections.namedtuple("Context", ["obj"])
        ctx = Context(obj={})
        ctx.obj["cfg"] = config
        config.data = {
            "profile": [
                {
                    "odoo-config-path": str(odoo_conf),
                    "working-directory": str(tmpdir),
                }
            ],
        }

        modules = complete.modules_complete(ctx, None, "")
        true_result = [str(mod.name) for mod in odoo_addons_mod[:-1]] + [
            str(mod.name) for mod in oca_server_mod[:-1]
        ]
        true_result.sort()
        assert modules == true_result

        modules = complete.modules_complete(ctx, None, "oca")
        true_result = [str(mod.name) for mod in oca_server_mod[:-1]]
        true_result.sort()
        assert modules == true_result


def test_file_completion():
    """Test for complete.files_completion"""
    with tempfile.TemporaryDirectory("-ociedoo-test") as tmpdir:
        # Populate a directory
        sqlfiles = []
        for f in ["a", "b", "c"]:
            sqlfiles.append(Path(tmpdir, f + ".sql"))
        for f in sqlfiles:
            f.touch()

        sqlgzfiles = []
        for f in ["a", "d", "e"]:
            sqlgzfiles.append(Path(tmpdir, f + ".sql.gz"))
        for f in sqlgzfiles:
            f.touch()

        otherfiles = []
        for f in ["a", "f", "g"]:
            otherfiles.append(Path(tmpdir, f))
        for f in otherfiles:
            f.touch()

        subdir = Path(tmpdir, "subdir")
        subdir.mkdir()

        subsqlfiles = []
        for f in ["a", "h", "i"]:
            subsqlfiles.append(Path(tmpdir, subdir, f + ".sql"))
        for f in subsqlfiles:
            f.touch()

        subsqlgzfiles = []
        for f in ["a", "h", "i"]:
            subsqlgzfiles.append(Path(tmpdir, subdir, f + ".sql.gz"))
        for f in subsqlgzfiles:
            f.touch()

        subotherfiles = []
        for f in ["x", "y", "z"]:
            subotherfiles.append(Path(tmpdir, subdir, f))
        for f in subotherfiles:
            f.touch()

        completefunc = complete.file_completion([".sql"])
        res = completefunc(None, None, tmpdir)
        assert set(res) == (
            {str(f) for f in sqlfiles} | {str(subdir) + os.sep}
        )
        res = completefunc(None, None, str(Path(tmpdir, "a")))
        assert set(res) == {str(f) for f in sqlfiles if "a" in str(f.name)}
        res = completefunc(None, None, str(Path(tmpdir, "sub")))
        assert set(res) == {str(subdir) + os.sep}
        res = completefunc(None, None, str(Path(tmpdir, subdir)))
        assert set(res) == {str(f) for f in subsqlfiles}
        res = completefunc(None, None, str(Path(tmpdir, subdir, "a")))
        assert set(res) == {str(f) for f in subsqlfiles if "a" in str(f.name)}

        completefunc = complete.file_completion([".sql", ".sql.gz"])
        res = completefunc(None, None, tmpdir)
        assert set(res) == (
            {str(f) for f in sqlfiles}
            | {str(f) for f in sqlgzfiles}
            | {str(subdir) + os.sep}
        )
        res = completefunc(None, None, str(Path(tmpdir, "a")))
        assert set(res) == (
            {str(f) for f in sqlfiles if "a" in str(f.name)}
            | {str(f) for f in sqlgzfiles if "a" in str(f.name)}
        )
        res = completefunc(None, None, str(Path(tmpdir, "sub")))
        assert set(res) == {str(subdir) + os.sep}
        res = completefunc(None, None, str(Path(tmpdir, subdir)))
        assert set(res) == (
            {str(f) for f in subsqlfiles} | {str(f) for f in subsqlgzfiles}
        )
        res = completefunc(None, None, str(Path(tmpdir, subdir, "a")))
        assert set(res) == (
            {str(f) for f in subsqlfiles if "a" in str(f.name)}
            | {str(f) for f in subsqlgzfiles if "a" in str(f.name)}
        )

        completefunc = complete.file_completion([".gz"])
        res = completefunc(None, None, tmpdir)
        assert set(res) == (
            {str(f) for f in sqlgzfiles} | {str(subdir) + os.sep}
        )
        res = completefunc(None, None, str(Path(tmpdir, "a")))
        assert set(res) == {str(f) for f in sqlgzfiles if "a" in str(f.name)}
        res = completefunc(None, None, str(Path(tmpdir, "sub")))
        assert set(res) == {str(subdir) + os.sep}
        res = completefunc(None, None, str(Path(tmpdir, subdir)))
        assert set(res) == {str(f) for f in subsqlgzfiles}
        res = completefunc(None, None, str(Path(tmpdir, subdir, "a")))
        assert set(res) == {
            str(f) for f in subsqlgzfiles if "a" in str(f.name)
        }

        completefunc = complete.file_completion()
        res = completefunc(None, None, tmpdir)
        assert set(res) == (
            {str(f) for f in sqlfiles}
            | {str(f) for f in sqlgzfiles}
            | {str(f) for f in otherfiles}
            | {str(subdir) + os.sep}
        )
        res = completefunc(None, None, str(Path(tmpdir, "a")))
        assert set(res) == (
            {str(f) for f in sqlfiles if "a" in str(f.name)}
            | {str(f) for f in sqlgzfiles if "a" in str(f.name)}
            | {str(f) for f in otherfiles if "a" in str(f.name)}
        )
        res = completefunc(None, None, str(Path(tmpdir, "sub")))
        assert set(res) == {str(subdir) + os.sep}
        res = completefunc(None, None, str(Path(tmpdir, subdir)))
        assert set(res) == (
            {str(f) for f in subsqlfiles}
            | {str(f) for f in subsqlgzfiles}
            | {str(f) for f in subotherfiles}
        )
        res = completefunc(None, None, str(Path(tmpdir, subdir, "a")))
        assert set(res) == (
            {str(f) for f in subsqlfiles if "a" in str(f.name)}
            | {str(f) for f in subsqlgzfiles if "a" in str(f.name)}
            | {str(f) for f in subotherfiles if "a" in str(f.name)}
        )

        # Relative to current path
        os.chdir(tmpdir)
        completefunc = complete.file_completion([".sql", ".sql.gz"])
        res = completefunc(None, None, "")
        assert set(res) == (
            {str(f.name) for f in sqlfiles}
            | {str(f.name) for f in sqlgzfiles}
            | {str(subdir.name) + os.sep}
        )
        res = completefunc(None, None, "a")
        assert set(res) == (
            {str(f.name) for f in sqlfiles if "a" in str(f.name)}
            | {str(f.name) for f in sqlgzfiles if "a" in str(f.name)}
        )
        res = completefunc(None, None, "sub")
        assert set(res) == {str(subdir.name) + os.sep}
        res = completefunc(None, None, str(subdir.name))
        assert set(res) == (
            {str(f.relative_to(tmpdir)) for f in subsqlfiles}
            | {str(f.relative_to(tmpdir)) for f in subsqlgzfiles}
        )
        res = completefunc(None, None, str(Path(subdir.name, "a")))
        assert set(res) == (
            {
                str(f.relative_to(tmpdir))
                for f in subsqlfiles
                if "a" in str(f.name)
            }
            | {
                str(f.relative_to(tmpdir))
                for f in subsqlgzfiles
                if "a" in str(f.name)
            }
        )
