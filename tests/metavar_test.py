# SPDX-FileCopyrightText: 2018 Coop IT Easy SC <https://coopiteasy.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test if pyproject.toml and special var in the package are the same."""

from pathlib import Path

import toml

import ociedoo as pkg


def test_pyproject():
    """Test pyproject file."""
    rootdir = Path(__file__).parent.parent.resolve()
    with (rootdir / "pyproject.toml").open() as file:
        pyproject = toml.loads(file.read())
    poetry = pyproject["tool"]["poetry"]
    assert poetry["name"] == pkg.__productname__
    assert poetry["version"] == pkg.__version__
    assert poetry["license"] == pkg.__license__
